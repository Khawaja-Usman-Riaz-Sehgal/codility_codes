#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
vector<int> solution(int N, vector<int> &A)
{
    vector<int> counter(N, 0);
    int max_value = 0;
    int increased_value = 0;
    int sizeA = A.size();
    for (int id = 0; id < sizeA; id++)
    {
        int index = A[id] - 1;
        if (A[id] > N)
        {
            increased_value = max_value;
        }
        else
        {
            counter[index] = max(counter[index], increased_value);
            counter[index]++;
            max_value = max(max_value, counter[index]);
        }
    }

    for (int id = 0; id < N; id++)
    {
        counter[id] = max(counter[id], increased_value);
    }

    return counter;
}
int main()
{
    vector<int> A = {3, 4, 4, 6, 1, 4, 4};

    int K = 5;

    vector<int> counter = solution(K, A);
    int sizeCounter = counter.size();
    for (int id = 0; id < sizeCounter; id++)
    {
        cout << counter[id] << " ";
    }
    cout << "\n";
    return 0;
}