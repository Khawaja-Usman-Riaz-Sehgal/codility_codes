#include <iostream>
#include <vector>
#include <string>
using namespace std;
enum nucleotide
{
    A = 1,
    C = 2,
    G = 3,
    T = 4
};
vector<int> solution(string &S, vector<int> &P, vector<int> &Q)
{
    int string_size = S.size();
    int p_size = P.size();

    vector<int> results(p_size, 0);

    vector<int> impactCount_A(string_size + 1, 0);
    vector<int> impactCount_C(string_size + 1, 0);
    vector<int> impactCount_G(string_size + 1, 0);
    vector<int> impactCount_T(string_size + 1, 0);

    int lastTotal_A = 0;
    int lastTotal_C = 0;
    int lastTotal_G = 0;
    int lastTotal_T = 0;

    for (int i = string_size - 1; i >= 0; --i)
    {
        switch (S[i])
        {
        case 'A':
            ++lastTotal_A;
            break;
        case 'C':
            ++lastTotal_C;
            break;
        case 'G':
            ++lastTotal_G;
            break;
        case 'T':
            ++lastTotal_T;
        };

        impactCount_A[i] = lastTotal_A; // 2,2,1,1,1,1,1,0
        impactCount_C[i] = lastTotal_C; // 3,2,2,2,1,0,0,0
        impactCount_G[i] = lastTotal_G; // 1,1,1,0,0,0,0,0
        impactCount_T[i] = lastTotal_T; // 1,1,1,1,1,1,0,0
    }

    for (int i = 0; i < p_size; ++i)
    {
        int pIndex = P[i]; // 2 5 0
        int qIndex = Q[i]; // 4 5 6

        int numA = impactCount_A[pIndex] - impactCount_A[qIndex + 1];
        int numC = impactCount_C[pIndex] - impactCount_C[qIndex + 1];
        int numG = impactCount_G[pIndex] - impactCount_G[qIndex + 1];
        int numT = impactCount_T[pIndex] - impactCount_T[qIndex + 1];

        if (numA > 0)
        {
            results[i] = A;
        }
        else if (numC > 0)
        {
            results[i] = C;
        }
        else if (numG > 0)
        {
            results[i] = G;
        }
        else if (numT > 0)
        {
            results[i] = T;
        }
    }

    return results;
}
int main()
{
    string S = "CCCCCAC";
    vector<int> P = {2, 5, 0};
    vector<int> Q = {4, 5, 6};
    vector<int> sol = solution(S, P, Q);
    for (int i = 0; i < sol.size(); i++)
    {
        cout << sol[i] << " ";
    }
    cout << "\n";

    return 0;
}