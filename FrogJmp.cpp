#include <iostream>

using namespace std;
int solution(int X, int Y, int D)
{
    int Distance = Y - X;
    int step = Distance / D;
    if (Distance % D != 0)
    {
        step++; //only add extra jump if remainder exists
    }
    return step;
}

int main()
{
    int sol = solution(10, 85, 30);
    cout << "minimal steps to be taken by Frog are : " << sol << endl;
    return 0;
}