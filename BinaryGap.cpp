#include <iostream>
using namespace std;
int solution(int num)
{
   int zeros = -1;
   int max_gap = 0;
    
    while (num > 0) {
        if ((num & 1) == 1) {
            zeros = 0;
        } else if (zeros != -1) {
            zeros++;
            if (max_gap < zeros) {
                max_gap = zeros;
            }
        }
        
        num = num >> 1;
    }
    
    return max_gap;
}
int main(int argc, const char** argv) {
    int num;
    cout<<"Enter Positive Integer to find Binary Gap"<<endl;
    cin>>num;
    int BinaryGap = solution(num);
    cout<<"Positive Integer => "<<num<<endl;
    cout<<"BinaryGap => "<<BinaryGap<<endl;

    return 0;
}