# Codility_Codes

This Repo will have codes for Codility tasks


- Codility
- ├── BinaryGap.cpp
- ├── CyclicRotation.cpp
- ├── FrogJmp.cpp
- ├── FrogRiverOne.cpp
- ├── OddOccurencesinArray.cpp
- ├── PermCheck.cpp
- ├── PermMissingElem.cpp
- ├── TapeEquilibrium.cpp
- ├── MaxCounter.cpp
- ├── MissingInteger.cpp
- ├── GenomicRangeQuery.cpp
- ├── MinAvgTwoSlice.cpp
- ├── PassingCars.cpp
- └── CountDiv.cpp
