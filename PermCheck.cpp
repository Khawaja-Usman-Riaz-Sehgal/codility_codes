#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int solution(vector<int> &A)
{
    sort(A.begin(), A.end()); //A = {1,3,4}
    int size = A.size();
    for (int i = 0; i < size; i++)
    {
        if (A[i] != (i + 1))
        {
            return 0;
        }
    }
    return 1;
}
int main()
{
    vector<int> A = {4, 3, 1};
    int solA = solution(A);
    if (solA == 1)
    {
        cout << "Array A is a permutation so function returns " << solA << endl;
    }
    else
    {
        cout << "Array A is a not permutation so function returns " << solA << endl;
    }
    return 0;
}