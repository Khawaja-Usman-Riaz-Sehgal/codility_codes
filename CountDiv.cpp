#include <iostream>
using namespace std;
int solution(int starting_integer, int ending_integer, int divisor)
{
    // Add 1 explicitly as starting_integer is divisible by divisor 
    if (starting_integer % divisor == 0) 
        return (ending_integer / divisor) - (starting_integer / divisor) + 1; 
  
    // starting_integer is not divisible by divisor 
    return (ending_integer / divisor) - (starting_integer / divisor); 
}

//int solution(int starting_integer, int ending_integer, int divisor) 
//{ return ending_integer/divisor - starting_integer/divisor + !(starting_integer%divisor); }

int main()
{
    int A = 6;
    int B = 11;
    int K = 2;
    int sol = solution(A, B, K);
    cout << "number of integers divisible by k in range [a..b] is/are : " << sol << endl;
    return 0;
}