#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A)
{
    int X = 0, count = 0;
    int N = A.size();
    for (int i = 0; i < N; i++)
    {
        if (A[i] == 0)
        {
            X++;
        }
        else
        {
            count += X;
        }

        if (count > 1000000000)
        {
            return -1;
        }
    }

    return count;
}
int main()
{
    vector<int> A = {0, 1, 0, 1, 1};
    int sol = solution(A);
    cout << "The number of passing cars on the road are " << sol << endl;
    return 0;
}