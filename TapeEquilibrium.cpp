#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A)
{
    int sum = 0;
    int N = A.size();
    int i;
    for (i = 0; i < N; i++)
    {
        sum += A[i];
    }

    int rsum = A[0];
    sum -= A[0];

    int diff = abs(sum - rsum);
    for (i = 1; i < N - 1; i++)
    {
        rsum += A[i];
        sum -= A[i];
        int temp = abs(sum - rsum);
        if (temp < diff)
        {
            diff = temp;
        }
    }

    return diff;
}
int main()
{
    vector<int> A = {3, 1, 2, 4, 3};
    int sol = solution(A);
    cout << "Minimum difference using Tape Equilibrium : " << sol << endl;
    return 0;
}