#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A) {
 
    unsigned int N = A.size() + 1;
	unsigned int total = (N * (N + 1)) / 2;
	unsigned int sum = 0;
	for (int i = 0; i < A.size(); i++)
		sum += A[i];

	unsigned int missing_element = total - sum;
	return missing_element;
}

int main()
{
    vector <int> A = {2,3,1,5};
    int sol = solution(A);
    cout<<"the missing element in a given permutation is : "<<sol<<endl;

    return 0;
}