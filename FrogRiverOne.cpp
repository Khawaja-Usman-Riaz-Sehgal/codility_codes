#include <iostream>
#include <vector>
using namespace std;
int solution(int leave_pos , vector<int> &leave_falling)
{   
    int size = leave_falling.size();
    vector<bool> flags(leave_pos-1);
    for(unsigned i = 0; i < size; ++i)
    {
        unsigned index = leave_falling[i] - 1;
        if(flags[index]==0)
        {
            flags[index] = true;
            if(--leave_pos==0)
            {
                return i;
            }
        }
    }
 
    return -1; 
}
int main()
{
    vector<int> A = {1,3,1,4,2,3,5,4};
    int x = 5;
    int sol = solution(x , A);
    cout<<"the earliest time when a frog can jump to the other side of a river is "<<sol<<" seconds"<<endl;
    return 0;
}