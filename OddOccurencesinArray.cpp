#include <vector>
#include <iostream>
using namespace std;


int solution(vector<int> &A) {
    // write your code in C++14 (g++ 6.2.0)
     int odd1 = 0;
 for(int i=0; i < A.size(); i++) {
    odd1 ^= A[i];
 }
 return odd1;
}

int main() {
    vector<int> A={9,3,9,3,9,7,9};
    //vector<int> A={5,5,5,3};
    int sol=solution(A);
    cout<<"Odd Occurences in Array are : "<<sol<<endl; 
    
    return 0;
}