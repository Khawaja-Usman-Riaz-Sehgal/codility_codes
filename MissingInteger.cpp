#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
int solution(vector<int> &A)
{
    int sizeA = A.size();
    int missing_int;
    vector<int> counter(sizeA);
    int sizeCounter = counter.size();
    // Count the items, only the positive numbers
    for (int i = 0; i < sizeA; i++)
    {
        int index = A[i] - 1;
        if (A[i] > 0 && A[i] <= sizeA)
        {
            counter[index]++;
        }
    }
    // Return the first number that has count 0
    for (int i = 0; i < sizeCounter; i++)
    {
        if (counter[i] == 0)
        {
            missing_int = i+1;
            return missing_int;
        }
    }
    // If no number has count 0, then that means all number in the sequence
    // appears so the next number not appearing is in next number after the
    // sequence.
    missing_int = sizeA + 1;
    return missing_int;
}
int main()
{
    vector<int> A = {-3,1,2,3,4};
    int sol = solution(A);
    cout << "The smallest positive integer that does not occur in a given sequence " << sol << endl;
    return 0;
}