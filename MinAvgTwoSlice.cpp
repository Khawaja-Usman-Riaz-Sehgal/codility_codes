#include <iostream>
#include <vector>
using namespace std;
int solution(vector<int> &A)
{
    int sizeA = A.size();
    if (sizeA < 2)
        return 0;
    int min_index = 0;
    double avg_min = (A[0] + A[1]) / 2; //3 , 2.66 , 2 . . . .!

    for (unsigned int i = 2; i < sizeA; i++)
    {

        double avg_three = (A[i - 2] + A[i - 1] + A[i]) / 3.0; //2.66 , 3 , 2.66 , 3.60 , 4.6
        double avg_two = (A[i - 1] + A[i]) / 2.0; //2 , 3.5 , 3 , 3 , 6.5

        if (avg_three < avg_min)
        {
            avg_min = avg_three;
            min_index = i - 2; // 0
        }
        if (avg_two < avg_min)
        {
            avg_min = avg_two;
            min_index = i - 1; // 1
        }
    }

    return min_index; // 1
}
int main()
{
    vector<int> A = {4, 2, 2, 5, 1, 5, 8};
    int sol = solution(A);
    cout << "The minimal average of any slice containing at least two elements is: " << sol << endl;
    return 0;
}